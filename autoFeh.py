import sys, os, subprocess
from pathlib import Path
import time as Time
from random import randint

time = 1
home = Path(os.path.expanduser("~"))
path = ""
rand = False

def chkPath(p):
    #Checks if a given folder contains
    #images in jpg format
    p = home / p
    if p.exists():
        if list(p.glob('*.jpg')) != []: #Puts every file with jpg format in a list and check if it's empty
            return True
    return False

try:
    if int(sys.argv[2]) < 1440:
        time = int(sys.argv[2]) * 60
    else:
        print("\033[93mNumber too big; going back to default time (5 minutes)\033[0m")
        time = 300
except ValueError:
    print("Invalid time argument")
    exit()

if chkPath(sys.argv[1]):
    path = Path(home/sys.argv[1])
else:
    print("Invalid path! (No images or directory doesn't exist)")
    exit()

if "random" in sys.argv:
    rand = True

imgs = [] #Contains the absolute paths to all the image files
for x in path.glob("*.jpg"): #Finds all the jpg files
    imgs.append(str(x))

def callFeh(i):
    subprocess.Popen(["feh", "--bg-scale", i])
    Time.sleep(time)

print("Path: %s\nFiles: %d\nTime: %d minutes\nRandom: %s\nPress ctrl+C to stop." % (path, len(imgs) ,time/60, str(rand)))

if rand:
    while True:
        callFeh(imgs[randint(0, len(imgs)-1)])
else:
    while True:
        for x in imgs:
            callFeh(x)
